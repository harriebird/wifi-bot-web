from django.conf import settings
from django.db import models
from django.utils import timezone


class Bot(models.Model):
    name = models.CharField(max_length=40)
    key = models.CharField(max_length=60)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class SensorData(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE)
    date_received = models.DateTimeField(default=timezone.now())
    air_quality = models.IntegerField()

    def __str__(self):
        return '{} @ {}'.format(self.bot.name, self.date_received)

