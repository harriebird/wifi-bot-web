from django.contrib import admin
from .models import Bot, SensorData

admin.site.register(Bot)
admin.site.register(SensorData)
