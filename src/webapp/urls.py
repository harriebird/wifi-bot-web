from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('send_data', views.receive_data, name='receive_data'),
    path('bot_data/<int:bot_id>', views.get_data, name='bot_data'),
    path('bot_data/<int:bot_id>/<str:req_date>', views.get_data, name='bot_data')
]
