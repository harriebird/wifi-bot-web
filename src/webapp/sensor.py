from .models import Bot, SensorData
from django.core.exceptions import ObjectDoesNotExist
import csv
from django.db.models import Avg
import os
from django.conf import settings
from django.utils import timezone
import datetime


def auth_sensor(request):
    name = request.POST.get('name', '')
    key = request.POST.get('key', '')

    try:
        sensor = Bot.objects.get(name=name, key=key)
    except ObjectDoesNotExist:
        return False

    return sensor


def process_data(request, bot):
    try:
        air_quality = request.POST.get('air_quality', '')
        sensor_data = SensorData(air_quality=air_quality, bot=bot)
        sensor_data.save()
        return True
    except:
        return False


def query_data(bot_id, record_date=None):
    if record_date:
        try:
            date_range = timezone.datetime.strptime(record_date, '%Y-%m-%d')
        except ValueError:
            date_range = timezone.datetime(timezone.datetime.now().year, timezone.datetime.now().month,
                                           timezone.datetime.now().day)
    else:
        date_range = timezone.datetime(timezone.datetime.now().year, timezone.datetime.now().month, timezone.datetime.now().day)

    sensor_data = SensorData.objects.filter(bot_id=bot_id, date_received__range=[date_range, date_range + datetime.timedelta(days=1)]).extra(select={'hour': 'hour(date_received)'}).values('hour', 'bot__name').annotate(air_quality=Avg('air_quality'))
    return parse_data(sensor_data)


def parse_data(data):
    hour = []
    air_quality = []
    for row in data:
        hour.append('{}:00'.format(row['hour']+8))
        air_quality.append(row['air_quality'])
        # parsed_data.append({'air_quality': row.air_quality, 'datetime': row.date_received.time().__str__()})
    return {'name': data[0]['bot__name'], 'hour': hour, 'air_quality': air_quality}
