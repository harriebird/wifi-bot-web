from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .sensor import auth_sensor, process_data, query_data


def index(request):
    return render(request, 'webapp/index.html')


@csrf_exempt
def receive_data(request):
    if request.method == 'POST':
        bot = auth_sensor(request)
        if bot:
            if process_data(request, bot):
                return JsonResponse({'message': 'data received'})
            else:
                return JsonResponse({'message': 'upload failed'})
        else:
            return JsonResponse({'message': 'unauthorized'}, status=401)
    else:
        return JsonResponse({'message': 'upload sensor data here'})


def get_data(request, bot_id, req_date=None):
    data = query_data(bot_id, req_date)
    return JsonResponse({'data': data})


